<?php
session_start()
?>

<!DOCTYPE html>
<html lang="fr">
<head>
<meta charset="UTF-8">
<title>Am·mot·dyte</title>
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
</head>
<body bgcolor="#EBFAEE" >
<div style="border-style:double; border-width:15px; border-color:#a1d92f; padding:3%; text-align:center; margin:4%; font-family:Arial; line-height:1.5em;">

<?php
if($_POST["who"]=="P"){
	// initialisation et affichage de l'entete, enregistrée dans la session
	$_SESSION['entete'] = "<b>Choisissez la première lettre.</b></br>";
	echo $_SESSION['entete']."<br/><br/>";

	// initialisation de la suite de lettres, enregistrée dans la session
	$_SESSION['mot']="";
}

if($_POST["who"]=="IA"){
	// initialisation de l'entete
	$_SESSION['entete'] = "<b>Voici la première lettre.</b></br></br>";
	
	// L'IA choisit la 1e lettre au hasard = initialisation du mot
	$alnb = rand ("1","26");		// 1 et 26 sont inclus
	$alphabet = array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
	$_SESSION['mot'] = $alphabet[$alnb-1];

	// mise à jour et affichage de l'entete
	$_SESSION['entete']=$_SESSION['entete'].$_SESSION['mot'];
	echo $_SESSION['entete']."<br/><br/>";
	
	// initialisation du tableau $tab regroupant tous les mots commençant par la suite de lettres $mot
	$_SESSION['tab']=array_slice($_SESSION["tabref"],$_SESSION["index"][$alphabet[$alnb-1]],$_SESSION["index"][$alphabet[$alnb]],$preserve_keys=true);
}
?>

<form method="post" action="continue.php">
	<input type="text" name="lettreJ" maxlength="1" size="1" autocomplete="off" autofocus>
	<input type="submit" />
</form>

</div>
</body>
</html>