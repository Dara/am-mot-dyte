<?php
session_start()
?>

<!DOCTYPE html>
<html lang="fr">
<head>
<meta charset="UTF-8">
<title>Am·mot·dyte</title>
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
</head>
<body bgcolor="#EBFAEE" >

<div style="border-style:double; border-width:15px; border-color:#a1d92f; padding:3%; text-align:center; margin:4%; font-family:Arial; line-height:1.5em;">

<?php
// mise à jour du mot
$lettreJ=enleve_accents($_POST["lettreJ"]);
function enleve_accents($texte) {
    return preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml|caron);~i', '$1', htmlentities($texte, ENT_QUOTES, 'UTF-8'));
}
$_SESSION['mot']=$_SESSION['mot'].strtolower($lettreJ);

// si p vient tout juste de commencer, initialise tab
if(strlen($_SESSION["mot"])==1){
	//trouve comment exprimer $lettreJ+1
	$alphabet = array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
	$n=array_search($lettreJ,$alphabet)+1;
	$_SESSION['tab']=array_slice($_SESSION["tabref"],$_SESSION["index"][$lettreJ],$_SESSION["index"][$alphabet[$n]],$preserve_keys=true);
}

// Regarde si le mot proposé par player existe
if(in_array($_SESSION['mot'],$_SESSION["tab"])){
	$_SESSION["marqueur"]="mot existant";
} else {
	$_SESSION["marqueur"]="mot non existant";
}

// mise à jour et affichage de l'entete
$_SESSION['entete']=$_SESSION['entete']."</br>".$_SESSION['mot'];
echo $_SESSION['entete']."<br/>";

// mise à jour de $tab (liste des mots commençant par la suite de lettres mot)
$tab=$_SESSION["tab"];		// récupère la précédente liste dans la session
$newtab=array();			// crée un tableau de stockage intermédiaire (passer par arary_splice est trop long)

$min=min(array_keys($tab));
$max=max(array_keys($tab));

for($i=$min;$i<=$max;$i++){		// parcours les mots de $tab
	if(array_key_exists($i,$tab)){
		$test=0;							// variable de contrôle : compte le nombre de lettres identiques entre mot et la ligne de $tab
		for($k=0;$k<=strlen($_SESSION['mot'])-1;$k++){ 	// parcours les lettres des mots
			if(substr($_SESSION['mot'],$k,1)==substr($tab[$i],$k,1)){
				$test++;					// pour chaque lettre identique, $test++
			}
		}
		if($test==strlen($_SESSION['mot']) && strlen($tab[$i])>strlen($_SESSION['mot'])){
			$newtab[$i]=$tab[$i];	// si toutes les lettres sont identiques et qu'il y a au moins une lettre de plus, on garde ce mot
		}
	}
}
$tab=$newtab;	// on remplace l'ancienne liste par la nouvelle
unset($newtab);	// supprime $newtab pour faire de la place

// Si player propose n'importe quoi, redirige vers tumens.php
if(count($tab)==0 && $_SESSION["marqueur"]=="mot non existant"){
	header('Location: tumens.php');
	exit();
}

// S'il n'y a pas d'autre mot, redirige vers tumens.php
if(count($tab)==0){
	header('Location: tumens.php');
	exit();
}

// S'il y a d'autres mots, réinitialisation du marqueur + pause dans l'affichage ?
$_SESSION["marqueur"]="pclick";

// Puis parmi les lettres suivantes possibles, l'IA en prend une au hasard.
// affichage du mot avec la lettre de l'IA

$h = rand(min(array_keys($tab)),max(array_keys($tab)));
while (!array_key_exists($h,$tab)){			// au cas où min-max n'est pas continu
	$h = rand(min(array_keys($tab)),max(array_keys($tab)));
}

$_SESSION['mot'] = $_SESSION['mot'].substr($tab[$h],strlen($_SESSION['mot']),1);
echo $_SESSION['mot']."<br/><br/>";
$_SESSION['entete']=$_SESSION['entete']."</br>".$_SESSION['mot'];

// mise à jour de $tab

$min=min(array_keys($tab));
$max=max(array_keys($tab));
$newtab=array();

for($i=$min;$i<=$max;$i++){
	if(array_key_exists($i,$tab)){
		if(substr($tab[$i],strlen($_SESSION['mot'])-1,1)==substr($_SESSION['mot'],strlen($_SESSION['mot'])-1,1)){
			$newtab[$i]=$tab[$i];
		}
	}
}

$_SESSION['tab']=$newtab;
?>

<form method="post" action="continue.php">
	<input type="text" name="lettreJ" maxlength="1" size="1" autocomplete="off" autofocus>
	<input type="submit" />	
</form>

</br>

<form method="post" action="tumens.php">
	<input type="submit" value=" Tu mens ! " style="font-size:1em;line-height:2em;" />
</form>
</div>
</body>
</html>